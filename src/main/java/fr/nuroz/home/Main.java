package fr.nuroz.home;

import fr.nuroz.home.commands.*;
import fr.nuroz.home.dao.NewDaoFile;
import fr.nuroz.home.dao.OldDaoFile;
import fr.nuroz.home.model.HomeParty;
import fr.nuroz.home.model.MyPlayer;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public final class Main extends JavaPlugin implements Listener
{
    public static HomeParty homeParty;
    public static File SAVEFILE;

    @Override
    public void onEnable()
    {
        SAVEFILE = new File("plugins/homes.save");

        // si le fichier homes.save exist
        if(SAVEFILE.exists() && SAVEFILE.isFile()) {

            //on recupère le fichier et initialise les paramètres ainsi que les homes de joueurs
            homeParty = NewDaoFile.getInstance().getHomeParty(SAVEFILE);

        } else {

            // on crée le fichier (initialisation du fichier pour le plugin)
            try {
                if(SAVEFILE.createNewFile()) {

                    File oldHomeDirectory = new File("plugins/home");

                    // si le dossier home existe alors on récupère les infos de l'ancien fonctionnement du plugin
                    if(oldHomeDirectory.exists() && oldHomeDirectory.isDirectory()) {
                        homeParty = OldDaoFile.getInstance().getHomeParty(oldHomeDirectory);

                    } else {
                        // sinon initialisation du plugin avec les paramètres par default
                        homeParty = new HomeParty(10000000, new ArrayList<>());
                    }

                    homeParty.save();
                } else {
                    System.out.println("HOME: impossible de crée le fichier de home");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // set event manager on plugin
        getServer().getPluginManager().registerEvents(this, this);

        // define commande and autoCompletion
        allHomeCompletion completionAllHome = new allHomeCompletion();

        getCommand("homes").setExecutor(new homes());
        getCommand("sethome").setExecutor(new sethome());

        PluginCommand homeCMD = getCommand("home");
        homeCMD.setExecutor(new home());
        homeCMD.setTabCompleter(completionAllHome);

        PluginCommand delHomeCMD = getCommand("delhome");
        delHomeCMD.setExecutor(new delhome());
        delHomeCMD.setTabCompleter(completionAllHome);

        getCommand("rule").setExecutor(new rule());

        System.out.println("HOME: le plugin vien de s'allumer");
    }

    @Override
    public void onDisable()
    {
        System.out.println("HOME: le plugin vien de s'éteindre");
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent e)
    {
        Player p = e.getPlayer();
        if(homeParty.getPlayer(p.getUniqueId()) == null) {
            homeParty.addPlayer(new MyPlayer(p.getUniqueId(), p.getName()));
            homeParty.save();
        }
    }
}
