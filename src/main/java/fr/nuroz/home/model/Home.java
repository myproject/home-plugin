package fr.nuroz.home.model;

import org.bukkit.Location;

public class Home {
    private String nom;
    private Location position;

    /**
     * constructor of Home class
     * @param nom
     * @param position
     */
    public Home(String nom, Location position) {
        this.nom = nom;
        this.position = position;
    }

    /**
     * getter on nom
     * @return
     */
    public String getNom() {
        return nom;
    }

    /**
     * getter on position
     * @return
     */
    public Location getPosition() {
        return position;
    }

    /**
     * setter on position
     * @param position
     */
    public void setPosition(Location position) {
        this.position = position;
    }

    /**
     * transform object to json
     * @return
     */
    public String toJson() {
        StringBuilder json = new StringBuilder();

        json.append("{\"nom\":\"");
        json.append(nom);
        json.append("\", \"x\":");
        json.append(position.getX());
        json.append(",\"y\":");
        json.append(position.getY());
        json.append(",\"z\":");
        json.append(position.getZ());
        json.append(",\"yaw\":");
        json.append(position.getYaw());
        json.append(",\"pitch\":");
        json.append(position.getPitch());
        json.append(", \"world\":");
        json.append(position.getWorld().getName());
        json.append("}");

        return json.toString();
    }
}
