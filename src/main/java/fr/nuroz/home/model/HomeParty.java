package fr.nuroz.home.model;

import fr.nuroz.home.Main;
import fr.nuroz.home.dao.NewDaoFile;

import java.util.ArrayList;
import java.util.UUID;

public class HomeParty {
    private int maxHome;
    private ArrayList<MyPlayer> players;

    /**
     * constructor of HomeParty class
     * @param maxHome
     * @param players
     */
    public HomeParty(int maxHome, ArrayList<MyPlayer> players) {
        this.maxHome = maxHome;
        this.players = players;
    }

    /**
     * getter on maxHome
     * @return
     */
    public int getMaxHome() {
        return maxHome;
    }

    /**
     * setter on maxHome
     * @param maxHome
     */
    public void setMaxHome(int maxHome) {
        this.maxHome = maxHome;
    }

    /**
     * player player in players by uuid
     * @param uuid
     * @return
     */
    public MyPlayer getPlayer(UUID uuid) {
        MyPlayer player = null;

        for(MyPlayer p : players) {
            if(p.getUuid().toString().equals(uuid.toString())) {
                player = p;
            }
        }

        return player;
    }

    /**
     * add player to MyPlayer list
     * @param player
     */
    public void addPlayer(MyPlayer player) {
        players.add(player);
    }

    /**
     * call NewDaoFile to save HomeParty content
     */
    public void save() {
        NewDaoFile.getInstance().setDataInFile(Main.SAVEFILE, toJson());
    }

    /**
     * transform object to json
     * @return
     */
    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\"settings\": {\"maxHome\":");
        json.append(maxHome);
        json.append("},\"players\": [");

        for(int i = 0; i < players.size(); ++i) {
            MyPlayer player = players.get(i);
            json.append(player.toJson());

            if(i != players.size() - 1) {
                json.append(",");
            }
        }

        json.append("]}");

        return json.toString();
    }
}