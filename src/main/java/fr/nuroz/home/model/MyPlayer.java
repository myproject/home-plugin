package fr.nuroz.home.model;

import java.util.ArrayList;
import java.util.UUID;

public class MyPlayer {
    private UUID uuid;
    private String name;
    private ArrayList<Home> homes;

    /**
     * Constructor of MyPlayer class
     * @param uuid
     * @param name
     * @param homes
     */
    public MyPlayer(UUID uuid, String name, ArrayList<Home> homes) {
        this.uuid = uuid;
        this.name = name;
        this.homes = homes;
    }

    /**
     * Constructor of MyPlayer class
     * @param uuid
     * @param name
     */
    public MyPlayer(UUID uuid, String name) {
        this.uuid = uuid;
        this.name = name;
        this.homes = new ArrayList<>();
    }

    /**
     * getter on uuid
     * @return
     */
    public UUID getUuid() {
        return uuid;
    }

    /**
     * getter on homes
     * @return
     */
    public ArrayList<Home> getHomes() {
        return homes;
    }

    /**
     * get home by name from homes
     * @param nom
     * @return
     */
    public Home getHomeByName(String nom) {
        Home home = null;

        for(Home h : homes) {
            if(h.getNom().equalsIgnoreCase(nom)) {
                home = h;
            }
        }

        return home;
    }

    /**
     * add Home object to homes list
     * @param home
     */
    public void addHome(Home home) {
        homes.add(home);
    }

    /**
     * remove Home object from homes list
     * @param home
     */
    public void deleteHome(Home home) {
        homes.remove(home);
    }

    /**
     * transform object to json
     * @return
     */
    public String toJson() {
        StringBuilder json = new StringBuilder();
        json.append("{\"uuid\":\"");
        json.append(uuid.toString());
        json.append("\", \"name\":\"");
        json.append(name);
        json.append("\", \"homes\": [");

        for(int i = 0; i < homes.size(); ++i) {
            Home home = homes.get(i);
            json.append(home.toJson());

            if(i != homes.size() - 1) {
                json.append(",");
            }
        }

        json.append("]}");
        return json.toString();
    }
}
