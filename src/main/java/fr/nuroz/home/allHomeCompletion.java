package fr.nuroz.home;

import fr.nuroz.home.model.Home;
import fr.nuroz.home.model.MyPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class allHomeCompletion implements TabExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        return false;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args)
    {
        // define result and player sender
        List<String> tabComplete = new ArrayList<>();
        Player p = (Player) sender;
        MyPlayer mp = Main.homeParty.getPlayer(p.getUniqueId());

        // add all home to auto-completion
        for(Home h : mp.getHomes()) {
            if(args[0].trim().equals("") || h.getNom().toUpperCase().startsWith(args[0].toUpperCase()) || h.getNom().equalsIgnoreCase(args[0])) {
                tabComplete.add(h.getNom());
            }
        }

        return tabComplete;
    }
}
