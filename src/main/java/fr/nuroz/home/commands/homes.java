package fr.nuroz.home.commands;

import fr.nuroz.home.Main;
import fr.nuroz.home.model.Home;
import fr.nuroz.home.model.MyPlayer;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class homes implements CommandExecutor
{

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(sender instanceof Player)
        {
            Player p = (Player) sender;

            // récuperation du joueur et du home
            MyPlayer mp = Main.homeParty.getPlayer(p.getUniqueId());
            ArrayList<Home> homes = mp.getHomes();

            // si le joueur n'a pas de home alors erreur au joueur sinon on lui affiche
            if(homes.size() == 0) {
                p.sendMessage("§4Vous avez Aucun home!");
            } else {
                homes.forEach(home -> {
                    Location position = home.getPosition();
                    p.sendMessage("§3" + home.getNom() + "§6: §3X:§6 " + Math.round(position.getX() * 100.0) / 100.0 + " §3Y:§6 " + Math.round(position.getY() * 100.0) / 100.0 + " §3Z:§6 " + Math.round(position.getZ() * 100.0) / 100.0 + " §3World:§6 " + position.getWorld().getName());
                });
            }
        }

        return false;
    }
}
