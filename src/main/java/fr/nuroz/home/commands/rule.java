package fr.nuroz.home.commands;

import fr.nuroz.home.Main;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class rule implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(sender instanceof Player)
        {
            Player p = (Player) sender;

            if(args.length == 2)
            {
                if(args[0].equalsIgnoreCase("maxHome"))
                {
                    // definition de la regle maxHome
                    Main.homeParty.setMaxHome(Integer.parseInt(args[1]));
                    p.sendMessage("§aNombre max de home(s): §6" + Main.homeParty.getMaxHome());
                }
                else
                {
                    p.sendMessage("§4La règle n'existe pas!");
                }
            }
            else
            {
                p.sendMessage("§4/rule maxhome <nombre>");
            }
        }

        return false;
    }
}
