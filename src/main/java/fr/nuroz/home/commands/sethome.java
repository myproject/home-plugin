package fr.nuroz.home.commands;

import fr.nuroz.home.Main;
import fr.nuroz.home.model.Home;
import fr.nuroz.home.model.MyPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class sethome implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(sender instanceof Player)
        {
            Player p = (Player) sender;

            if(args.length == 1)
            {
                // récuperation du joueur et du home
                MyPlayer mp = Main.homeParty.getPlayer(p.getUniqueId());
                Home home = mp.getHomeByName(args[0]);

                // si le home n'éxiste pas on le créé (si le nombre max home n'est pas atteint) sinon on le modifie
                if(home == null) {
                    if(mp.getHomes().size() < Main.homeParty.getMaxHome()) {
                        home = new Home(args[0], p.getLocation());
                        mp.addHome(home);
                        Main.homeParty.save();
                        p.sendMessage("§aHome crée!");
                    } else {
                        p.sendMessage("§4Nombre de home maximal atteint!");
                    }
                } else {
                    home.setPosition(p.getLocation());
                    Main.homeParty.save();
                    p.sendMessage("§aHome modifié!");
                }
            }
            else
            {
                p.sendMessage("§4/sethome <nom>");
            }

        }
        return false;
    }
}
