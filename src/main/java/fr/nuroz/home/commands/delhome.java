package fr.nuroz.home.commands;

import fr.nuroz.home.Main;
import fr.nuroz.home.model.Home;
import fr.nuroz.home.model.MyPlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class delhome implements CommandExecutor
{
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if(sender instanceof Player)
        {
            Player p = (Player) sender;

            if(args.length == 1)
            {
                // récuperation du joueur et du home
                MyPlayer mp = Main.homeParty.getPlayer(p.getUniqueId());
                Home home = mp.getHomeByName(args[0]);

                // si le home existe alors on le supprime sinon message d'erreur au joueur
                if(home != null) {
                    mp.deleteHome(home);
                    Main.homeParty.save();
                    p.sendMessage("§aHome suprimé!");
                } else {
                    p.sendMessage("§4Le home n'existe pas!");
                }
            }
            else
            {
                p.sendMessage("§4/delhome <nom>");
            }
        }
        return false;
    }
}
