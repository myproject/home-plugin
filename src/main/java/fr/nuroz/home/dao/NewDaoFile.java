package fr.nuroz.home.dao;

import fr.nuroz.home.model.Home;
import fr.nuroz.home.model.HomeParty;
import fr.nuroz.home.model.MyPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.UUID;

public class NewDaoFile extends DaoFile {
    private static NewDaoFile instance;

    /**
     * getter on instance NewDaoFile
     * @return
     */
    public static NewDaoFile getInstance() {
        if(instance == null) {
            instance = new NewDaoFile();
        }

        return instance;
    }

    /**
     * constructor of NewDaoFile class
     */
    private NewDaoFile() {}

    /**
     * return homeParty object of file
     * @param fichier
     * @return
     */
    @Override
    public HomeParty getHomeParty(File fichier) {

        // parse json
        String jsonHomeParty = readFileAsString(fichier);
        JSONObject jsonObject = new JSONObject(jsonHomeParty);
        JSONObject settings = jsonObject.getJSONObject("settings");

        // define settings
        int maxHome = settings.getInt("maxHome");

        // get players from json
        ArrayList<MyPlayer> players = new ArrayList<>();
        JSONArray jsonPlayers = jsonObject.getJSONArray("players");
        for(int i = 0; i < jsonPlayers.length(); ++i) {
            JSONObject player = jsonPlayers.getJSONObject(i);

            // get home from json
            ArrayList<Home> homes = new ArrayList<>();
            JSONArray jsonHomes = player.getJSONArray("homes");
            for(int j = 0; j < jsonHomes.length(); j++) {

                // set home in player object
                JSONObject home = jsonHomes.getJSONObject(j);
                Location position = new Location(Bukkit.getWorld(home.getString("world")), home.getDouble("x"), home.getDouble("y"), home.getDouble("z"), home.getFloat("yaw"), home.getFloat("pitch"));
                homes.add(new Home(home.getString("nom"), position));
            }

            // define player in HomeParty object
            players.add(new MyPlayer(UUID.fromString(player.getString("uuid")), player.getString("name"), homes));
        }

        return new HomeParty(maxHome, players);
    }
}
