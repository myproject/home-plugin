package fr.nuroz.home.dao;

import fr.nuroz.home.model.HomeParty;

import java.io.*;
import java.util.ArrayList;

public abstract class DaoFile {

    /**
     * read file and return a list of line in String
     * @param fichier
     * @return
     */
    public ArrayList<String> readFileAsStringArray(File fichier) {
        ArrayList<String> lignes = new ArrayList<>();

        try {
            FileReader fr = new FileReader(fichier);
            BufferedReader br = new BufferedReader(fr);
            String line;

            while((line = br.readLine()) != null)
            {
                lignes.add(line);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return lignes;
    }

    /**
     * read file and return a string of all content
     * @param fichier
     * @return
     */
    public String readFileAsString(File fichier) {
        StringBuilder data = new StringBuilder();

        ArrayList<String> lignes = readFileAsStringArray(fichier);
        for(String ligne : lignes) {
            data.append(ligne);
        }

        return data.toString();
    }

    /**
     * return homeParty object of file
     * @param fichier
     * @return
     */
    public abstract HomeParty getHomeParty(File fichier);

    /**
     * set content in file
     * @param fichier
     * @param data
     */
    public void setDataInFile(File fichier, String data) {
        try{
            FileWriter fw = new FileWriter(fichier);
            fw.write(data);
            fw.close();
        } catch(Exception e) {
            e.printStackTrace();
        }
    };
}
