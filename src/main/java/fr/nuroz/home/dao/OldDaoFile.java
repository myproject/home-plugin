package fr.nuroz.home.dao;

import fr.nuroz.home.model.Home;
import fr.nuroz.home.model.HomeParty;
import fr.nuroz.home.model.MyPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.io.File;
import java.util.ArrayList;
import java.util.UUID;

public class OldDaoFile extends DaoFile {
    private static OldDaoFile instance;

    /**
     * getter on instance OldDaoFile
     * @return
     */
    public static OldDaoFile getInstance() {
        if(instance == null) {
            instance = new OldDaoFile();
        }

        return instance;
    }

    /**
     * constructor of OldDaoFile class
     */
    private OldDaoFile() {}

    /**
     * return homeParty object of file
     * @param fichier
     * @return
     */
    @Override
    public HomeParty getHomeParty(File fichier) {
        ArrayList<MyPlayer> allPlayers = new ArrayList<>();

        File[] listOfFiles = fichier.listFiles();

        for (File file : listOfFiles) {
            if (file.isFile()) {
                allPlayers.add(getMyPlayerByFile(file));
            }
        }

        return new HomeParty(10000000, allPlayers);
    }

    /**
     * return all MyPlayer object from file
     * @param fichier
     * @return
     */
    private MyPlayer getMyPlayerByFile(File fichier) {
        // read file
        ArrayList<String> lignes = readFileAsStringArray(fichier);

        // define all result needed
        UUID uuid = null;
        String nom = null;
        ArrayList<Home> homes = new ArrayList<>();

        // for on lignes
        for(int i = 0; i < lignes.size(); ++i) {
            String ligne = lignes.get(i);

            // si i == 0 alors on récupère le nom et l'uuid du joueur
            // exemple de ligne 0 des encien fichier:
            // ->NRZ<- / NuRoZ / 9a940e67-067d-4e72-82e8-21b391004a1e /1/->NRZ<-
            if(i == 0) {
                ligne = ligne.replace(" ", "");
                String[] ligneSplit = ligne.split("/");
                nom = ligneSplit[1];
                uuid = UUID.fromString(ligneSplit[2]);
            } else if(i != 1) {
                // sinon si la ligne n'est pas 1 (on oublie la 1 car useless)
                // on recupère le home de la ligne
                // exemple de ligne de home:
                // ici,-6.028943950875306,81.0,12.699999988079071,-10.005973,33.00397,world
                String[] ligneSplit = ligne.split(",");
                String nomHome = ligneSplit[0];
                Location position = new Location(Bukkit.getWorld(ligneSplit[6]), Double.parseDouble(ligneSplit[1]), Double.parseDouble(ligneSplit[2]), Double.parseDouble(ligneSplit[3]), Float.parseFloat(ligneSplit[4]), Float.parseFloat(ligneSplit[5]));
                homes.add(new Home(nomHome, position));
            }
        }
        return new MyPlayer(uuid, nom, homes);
    }
}
